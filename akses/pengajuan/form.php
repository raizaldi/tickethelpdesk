
<?php
include 'access/db.php';
//error_reporting(0);
session_start();
if(!isset($_SESSION['username']) || $_SESSION['level']!="user"){
	header('location:login.php');
}

$sql = "SELECT max(no_ticket) as no FROM ticket_job";
$result = $db->query($sql);
  while($row = mysqli_fetch_assoc($result)){
		$kd = $row['no'];
		$subs = substr($kd,6);
		$subs = $subs+1;
		$kd = "TIKET-".$subs; 		
  }
?>

<form role="form" action="" method="POST">
            <div class="col-lg-12">
                
                <div class="form-group">
                    <label for="InputName">Nomor Ticket</label>
                    <div class="input-group">
                        <input type="text" class="form-control" name="nomor" id="nomor" value="<?php echo $kd; ?>" disabled>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
				            
				<div class="form-group">
                    <label for="InputEmail">Cabang</label>
                    <div class="input-group">
                        <input type="text" class="form-control"  name="cabang" value="<?php echo $_SESSION['cabang'];?>" disabled>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
				
                <div class="form-group">
                    <label for="InputEmail">Kendala</label>
                    <div class="input-group">
						<select  class="form-control" name="kendala">
						 <option value="">== Pilih kendala ==</option>
						  <option value="jaringan">Jaringan</option>
						   <option value="komputer">Laptop/Komputer</option>
							<option value="printer">Printer</option>
						</select>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
				
				<div class="form-group">
                    <label for="InputMessage">Keterangan</label>
                    <div class="input-group">
                        <textarea name="issue" id="issue" class="form-control" rows="5"></textarea>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
				
				<div class="form-group">
                    <label for="InputEmail">PIC</label>
                    <div class="input-group">
						<select  class="form-control" name="pic">
						 <option value="">== Pilih PIC ==</option>
						  <option value="Rizki">Rizki</option>
						  <option value="Danu">Danu</option>
						  <option value="Akbar">Akbar</option>
						</select>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                
				<div class="form-group">
                    <label for="InputEmail">Pelapor</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="pelapor" name="pelapor" >
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
				
				<div class="form-group">
                    <label for="InputEmail">Prioritas</label>
                    <div class="input-group">
						<select  class="form-control" name="prioritas">
						 <option value="">== Pilih Prioritas ==</option>
						  <option value="High">High < 1 Hari</option>
						  <option value="Medium">Medium < 2 Hari   </option>
						  <option value="Low">Low < 3 Hari </option>
						</select>
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
				
				<input type="submit" name="submit" id="submit" value="Simpan" class="btn btn-info pull-right">
            
			</div>
</form>

<?php
if(isset($_POST['submit'])){
	$nomor = $kd;
	$cbg = $_SESSION['cabang'];
	$issue = $_POST['issue'];
	$kendala = $_POST['kendala'];
	$pic = $_POST['pic'];
	$prioritas = $_POST['prioritas'];
	$pelapor = $_POST['pelapor'];
	
	$sql = "INSERT INTO `tiket`.`ticket_job` (`no_ticket`, `tanggal_ticket`, `cabang`, `issue`, `kendala`, `pelapor`, `status`, `pic`, `time_lapor`) VALUES ('$nomor', NOW(), '$cbg', '$issue', '$kendala', '$pelapor', 'PROGRESS', '$pic', NOW());";
	$result = $db->query($sql);
	if($result){
		echo "
			<script type='text/javascript'>
				window.location = '?module=laporan';
			</script>
		";
	}else{
		echo "Simpanan Gagal";
	}
	}
?>