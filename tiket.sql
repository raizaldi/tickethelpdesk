-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 24 Jun 2016 pada 18.59
-- Versi Server: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tiket`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_solusi`
--

CREATE TABLE IF NOT EXISTS `tb_solusi` (
`no_solusi` int(11) NOT NULL,
  `no_ticket` varchar(20) NOT NULL,
  `tgl_solusi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `time_solusi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `solusi` text NOT NULL,
  `pic` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tb_solusi`
--

INSERT INTO `tb_solusi` (`no_solusi`, `no_ticket`, `tgl_solusi`, `time_solusi`, `solusi`, `pic`) VALUES
(1, 'AC-1-20160604', '2016-06-04 16:24:05', '2016-06-04 16:24:05', 'butuh dibersihkan memory', 'Akbar'),
(2, 'TIKET-1', '2016-06-06 19:16:37', '2016-06-06 19:16:37', 'bersihkan memory', 'Akbar'),
(3, 'TIKET-4', '2016-06-07 14:11:45', '2016-06-07 14:11:45', 'power suplynya rusak dan perlu diganti', 'Danu');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ticket_job`
--

CREATE TABLE IF NOT EXISTS `ticket_job` (
  `no_ticket` varchar(20) NOT NULL,
  `tanggal_ticket` date NOT NULL,
  `cabang` varchar(10) NOT NULL,
  `issue` text NOT NULL,
  `kendala` varchar(15) NOT NULL,
  `pelapor` varchar(30) NOT NULL,
  `status` varchar(30) NOT NULL,
  `pic` varchar(30) NOT NULL,
  `time_lapor` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ticket_job`
--

INSERT INTO `ticket_job` (`no_ticket`, `tanggal_ticket`, `cabang`, `issue`, `kendala`, `pelapor`, `status`, `pic`, `time_lapor`) VALUES
('TIKET-1', '2016-06-07', 'bogor', 'layar komputer berwarna biru', 'komputer', 'shinta', 'Problem Solved', 'Akbar', '01:48:32'),
('TIKET-2', '2016-06-07', 'bogor', 'paper jam', 'printer', 'jodie', 'PROGRESS', 'Danu', '01:52:43'),
('TIKET-3', '2016-06-07', 'bogor', 'tidak bisa buka akses email', 'jaringan', 'bagus', 'PROGRESS', 'Rizki', '01:55:50'),
('TIKET-4', '2016-06-07', 'bogor', 'tidak mau muncul windows', 'komputer', 'dina', 'Problem Solved', 'Danu', '21:10:55');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `cabang` varchar(50) NOT NULL,
  `level` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `cabang`, `level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'HO', 'admin'),
(2, 'bogor', '624a4069cc0699621bb6db6a818b8c2a', 'bogor', 'user'),
(3, 'jakarta', '629ab14fab772d78a58eea752bdfc0dc ', 'Jakarta', 'user'),
(4, 'depok', 'b2cc2da61aa6b4c02d4cd367f2920763 ', 'Depok', 'user'),
(5, 'serang', '38097912f54479cf0e409da540b37f0d', 'Serang', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_solusi`
--
ALTER TABLE `tb_solusi`
 ADD PRIMARY KEY (`no_solusi`);

--
-- Indexes for table `ticket_job`
--
ALTER TABLE `ticket_job`
 ADD PRIMARY KEY (`no_ticket`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_solusi`
--
ALTER TABLE `tb_solusi`
MODIFY `no_solusi` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
