<br/><br/><br/><br/>

<table id="dataTable" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>No Ticket</th>
				<th>Cabang</th>
				<th>Issue</th>
				<th>Pelapor</th>
				<th>Status</th>
				<th>Pic</th>
				<th>Tanggal Masalah</th>
				<th>Tanggal Selesai</th>
			</tr>
		</thead>
		<tbody>
		<?php
			
			$sql = "SELECT a.`no_ticket` as `no`, a.`tanggal_ticket` as `tglajuin`, a.`cabang` as `cbg`, a.`issue` as `issue`, a.`pelapor` as `lapor`, a.`status` as `status`, a.`pic` as `pic`, a.`time_lapor` as `time`,b.`tgl_solusi` as `tglbener`, b.`time_solusi` as `tm`, b.`solusi` as `solusi`, b.`pic` as `pc` FROM `ticket_job` a inner join `tb_solusi` b on a.`no_ticket`=b.`no_ticket`";
			$result = $db->query($sql);
			while($row = mysqli_fetch_assoc($result)){ ?>
			<tr>
				<td><?=$row['no'];?></td>
				<td><?=$row['cbg'];?></td>
				<td><?=$row['issue'];?></td>
				<td><?=$row['lapor'];?></td>
				<td><?=$row['status'];;?></td>
				<td><?=$row['pc'];?></td>
				<td><?php $tgl = $row['tglajuin'];
						echo date("d M Y", strtotime($tgl));?></td>
				<td><?php $tgl = $row['tglbener'];
						echo date("d M Y", strtotime($tgl));?></td>
			</tr>
			<?php
			  }
			?>
		</tbody>
	</table>
	
	<script type="text/javascript">
        $(document).ready(function () {
            var table = $('#dataTable').dataTable();
            var tableTools = new $.fn.dataTable.TableTools(table, {
                'aButtons': [
                    {
                        'sExtends': 'xls',
                        'sButtonText': 'Save to Excel',
                        'sFileName': 'Data.xls'
                    },
                    {
                        'sExtends': 'print',
                        'bShowAll': true,
                    },
                    {
                        'sExtends': 'pdf',
                        'bFooter': false
                    },
                    'copy',
                    'csv'
                ],
                'sSwfPath': '//cdn.datatables.net/tabletools/2.2.4/swf/copy_csv_xls_pdf.swf'
            });
            $(tableTools.fnContainer()).insertBefore('#dataTable_wrapper');
        });
    </script>

