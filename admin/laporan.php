<br/><br/><br/><br/>

<table id="dataTable" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>No Ticket</th>
				<th>Issue</th>
				<th>Status</th>
				<th>Pic</th>
				<th>Pelapor</th>
				<th>Tanggal Pelapor</th>
				<th>Waktu Pelapor</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
		<?php
			$sql = "SELECT `no_ticket`, `tanggal_ticket`, `cabang`, `issue`, `kendala`, `pelapor`, `status`, `pic`, `time_lapor` FROM `ticket_job` where `status`='PROGRESS' order by `no_ticket` desc";
			$result = $db->query($sql);
			while($row = mysqli_fetch_assoc($result)){ ?>
			<tr>
				<td><?=$row['no_ticket'];?></td>
				<td><?=$row['issue'];?></td>
				<td><?=$row['status'];;?></td>
				<td><?=$row['pic'];?></td>
				<td><?=$row['pelapor'];?></td>
				<td><?php $tgl = $row['tanggal_ticket'];
						echo date("d F Y", strtotime($tgl));?></td>
				<td><?php $tgl = $row['time_lapor'];
						echo date("h:i:sa", strtotime($tgl));?></td>
					<td><a href="index.php?module=ps&s=<?php echo $row['no_ticket'];?>">Solved</td>
			</tr>
			<?php
			  }
			?>
		</tbody>
	</table>
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#dataTable').dataTable();
		} );
	</script>

