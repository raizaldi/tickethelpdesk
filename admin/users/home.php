<br/><br/><br/><br/>

<a class="btn btn-success" href="?module=users/tambah" role="button">Tambah</a>
<br/><br/>
<table id="dataTable" class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Id User</th>
				<th>Username</th>
				<th>Password</th>
				<th>Cabang</th>
				<th>Level</th>
				<th>Aksi</th>
			</tr>
		</thead>
		<tbody>
		<?php
			include '../access/db.php';
			$sql = "SELECT `id`, `username`, `password`, `cabang`, `level` FROM `users`";
			$result = $db->query($sql);
			while($row = mysqli_fetch_assoc($result)){ ?>
			<tr>
				<td><?=$row['id'];?></td>
				<td><?=$row['username'];?></td>
				<td><?=$row['password'];;?></td>
				<td><?=$row['cabang'];?></td>
				<td><?=$row['level'];?></td>
				<td><a href="index.php?module=users/ubah&u=<?= $row['id'];?>">Ubah</a> |
				    <a href="index.php?module=users/hapus&h=<?= $row['id'];?>">Hapus</a>
				</td>
			</tr>
			<?php
			  }
			?>
		</tbody>
	</table>
	
	<script type="text/javascript" charset="utf-8">
		$(document).ready(function() {
			$('#dataTable').dataTable();
		} );
	</script>

