<?php

include 'access/db.php';
error_reporting(0);
session_start();
if(!isset($_SESSION['username']) || $_SESSION['level']!="user"){
	header('location:login.php');
}
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Sistem Informasi Helpdesk</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<style type='text/css'>
		body {
  padding-top: 50px;
}
.starter-template {
  padding: 40px 15px;
  text-align: left;
}
	</style>
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">Sistem Informasi Helpdesk</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
		  <li><a href="index.php">Home</a></li>
		  <li><a href="?module=form">Tiket Pengajuan</a></li>
		  <li><a href="?module=laporan">Laporan Pengajuan</a></li>
		 <li><a href="logout.php">Logout</a></li>
          </ul>
		  <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="">Welcome To &nbsp;<?php echo $_SESSION['username'];?></a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
      <div class="row">
      <div class="starter-template">