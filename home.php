<center>
<h1>User Guide Pengajuan Masalah</h1>
<img src="asset/login.png" class="img-responsive" alt="Responsive image">
</center>
<br/>
<p>
Login dengan username dan password yang sudah diberikan dan akan muncul seperti gambar dibawah ini. Akan muncul dashboard dengan 3 menu yaitu tampilan home awal, new ticket dan laporan pengajuan.
</p>
<br/>
<img src="asset/home.png" class="img-responsive" alt="Responsive image">
<br/>
Lalu klik new ticket untuk mengajukan permasalahan yang terjadi <br/>
<img src="asset/form.png" class="img-responsive" alt="Responsive image">
<br/>
</center>
<b>Keterangan</b><br/>
1.Nomer Ticket : Nomer pengajuan yang otomatis terbuat didalam sistem <br/>
2.Cabang : nama cabang yang login akan otomatis tampil <br/>
3.Kendala : Pilih jenis kendala yang dihadapi <br/>
4.Keterangan : sebutkan masalah yang terjadi dicabang yang berhubungan dengan masalah jaringan dan masalah Solomon <br/>
5.Pic : nama yang nantinya akan bertanggung jawab akan masalah tersebut <br/>
6.Pelapor : nama user yang melaporkan masalah tersebut dan diharuskan dengan nama pribadi tidak boleh menggunaan nama samaran <br/>
7.Prioritas : memberikan nilai pada permasalahan tersebut sesulit apa masalah agar bisa dijadikan acuan bagi pic untuk menyelesaikan masalah tersebut<br/>
<br/><br/><br/>
Dan untuk melihat hasil proses pengajuan yang dilaporkan bisa lihat di Laporan pengajuan <br/><br/>
<center>
<img src="asset/laporan.png" class="img-responsive" alt="Responsive image">
</center><br/><br/>
<p>
Ditabel muncul hasil dari pengajuan yang telah dibuat dan muncul status PROGRESS yang berarti masalah sedang dalam proses dan jika sudah selesai maka akan muncu tulisan solved dan jika ada masalah yang berhubungan dengan jaringan maka akan buatkan masalah tersebut ketika jaringan sudah selesai.
</p>


